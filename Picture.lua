local self = {}

self.width = 8 
self.height = 6

self.pixels = {}

for i=1, self.width*self.height do
	table.insert(self.pixels, 0xFFFFFFFF)
end

function self.fill(color)
	for i=1, self.width*self.height do
		self.pixels[i] = color 
	end
end

function self.setPixel(x, y, color)
	self.pixels[x+self.width*(y-1)] = color
end

function self.exportPpm(filename)
	local ppm_header = "P6\n"..self.width.." "..self.height.." 255\n"
	local file = io.open(filename, "wb")

	io.input(file)
	for i = 1, #ppm_header do 
		file:write(ppm_header:sub(i,i))
	end

	for i=1, #self.pixels do
		file:write(string.char((self.pixels[i]>>8*2) & 0xFF))
		file:write(string.char((self.pixels[i]>>8*1) & 0xFF))
		file:write(string.char((self.pixels[i]>>8*0) & 0xFF))
	end
	io.close(file)
end

function self.flip()
end

function self.flop()
end

return self
