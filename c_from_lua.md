# call c function from lua

## foo.c
```
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

static int foo(lua_State *L)
{
  int a = luaL_checknumber(L, 1);
  int b = luaL_checknumber(L, 2);

  lua_pushnumber(L, a + b);

  return 1;    
}

luaL_Reg foolib[] = {
  {"foo", foo},
  {NULL, NULL}
};

int luaopen_foo(lua_State *L)
{
  luaL_newlib(L, foolib);
  return 1;
}

```



## hw.lua
```
print(require "foo".foo(10, 11))
```

## compile
* compile : 
```
gcc -shared -fpic `pkg-config lua5.4 --cflags` -o foo.so foo.c `pkg-config lua5.4 --libs`
```

## run : 
```
lua hw.lua
```

## notes
* download lua from official website : https://www.lua.org/download.html
* lua c header files are in the tar (lua.h, lualib.h, lauxlib.h)