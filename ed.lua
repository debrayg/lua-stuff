-- minimalist text editor, inspired by : https://www.youtube.com/watch?v=gnvDPCXktWQ

function main()
	local file = io.open(arg[1], "r")

	local file_content = {}
	for line in file:lines() do table.insert(file_content, line) end
	io.close(file)

	for i in pairs(file_content) do print(file_content[i]) end

	local current_line = 0
	current_line = io.read("*n")

	io.read("*l") -- flush \n from buffer

	file_content[current_line] = io.read("*l")
	
	local file2 = io.open(arg[1], "w")

	for i in pairs(file_content) do 
		file2:write(file_content[i])
		file2:write("\n")
	end

	io.close(file2)
end


main()