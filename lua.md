# LUA BASICS

## READ FILE
```
file2 = io.open("test2.lua", "r")
print(file2:read())
io.close(file2)
```

### WRITE FILE
```
file = io.open("test2.lua", "w")
io.input(file)
file:write("hw")
io.close(file)
```

## WRITE BYTE TO FILE
```
file3 =local stack = require("stack")

stack.push(1)
stack.push(2)
stack.push(3)

assert(stack.pop() == 3)
assert(stack.pop() == 2)
stack.push(72)
assert(stack.pop() == 72)
assert(stack.pop() == 1)
assert(stack.pop() == nil)
 io.open("test3", "wb")
file3:write(string.char(0xFF)) -- OK
io.close(file3)
```

## print without newline
```
io.write("hello")
```


## for loop, array of bytes 
```
file = io.open("raw", "wb")
io.input(file)

bytes = {0xFF, 0xAA, 0x03}

for i in pairs(bytes) do
        file:write(string.char(bytes[i]))
end
io.close(file)
```


## function for loop bytes
```
function writeBytes(filename, bytes)
        file = io.open("raw", "wb")
        io.input(file)
        for i in pairs(bytes) do
                file:write(string.char(bytes[i]))
        end
        io.close(file)
        return 0
end

writeBytes("raw", {0xCC, 0xAB, 0xFF, 0x01})
```


## arrays

```
local headers = {"names", "city", "job"}
local names = {"Luke", "Leia", "Anakin"}
local city = {"Tatooin", "Aldraan", "Moustafaar"}
local jobs = {"Jedi", "Princess", "Sith"}


local file = io.open("SW.csv", "w")
io.input(file)

for i=1, #headers do
        file:write(headers[i])
        file:write(";")
end

file:write("\n")

for i=1, #names do
        file:write(names[i])
        file:write(";")
        file:write(city[i])
        file:write(";")
        file:write(jobs[i])
        file:write("\n")
end

io.close(file)
```




## bytewise
```
local pixel = 0xFF9ABCDE
assert( (pixel>>8*3) & 0xFF == 0xFF )
assert( (pixel>>8*2) & 0xFF == 0x9A )
assert( (pixel>>8*1) & 0xFF == 0xBC )
assert( (pixel>>8*0) & 0xFF == 0xDE )
```


## tables
```
local foo = {"foo", "bar", "foobar"}

assert(foo[1] == "foo")
assert(foo[2] == "bar")
assert(foo[3] == "foobar")

table.insert(foo, "barfoo")

assert(foo[4] == "barfoo")

```


## tables 2
```
local cart = {}

cart["tomatoes"] = 7
cart["apples"] = 10
cart["citrus"] = 4

assert(cart["tomatoes"] == 7)
assert(cart["apples"] == 10)
assert(cart["citrus"] == 4)

cart["tomatoes"] = cart["tomatoes"] + 1
assert(cart["tomatoes"] == 8
```







## browse string
```
local str = "Hello world"
for i = 1, #str do  
        print(str:sub(i,i))
end
```







## picture, module

```
picture.pixels = {}

function picture.fill()
        for i=1,picture.width*picture.height do
                table.insert(picture.pixels, 0xFFFF0000)
        end
end

function picture.exportPpm(filename)
        local ppm_header = "P6\n"..picture.width.." "..picture.height.." 255\n"
        local file = io.open(filename, "wb")

        io.input(file)
        for i = 1, #ppm_header do 
                file:write(ppm_header:sub(i,i))
        end

        for i=1, #picture.pixels do
                file:write(string.char((picture.pixels[i]>>8*2) & 0xFF))
                file:write(string.char((picture.pixels[i]>>8*1) & 0xFF))
                file:write(string.char((picture.pixels[i]>>8*0) & 0xFF))
        end
        io.close(file)
end

return picture

-- 

local pic = require("picture")
pic.width = 100local foo = {"foo", "bar", "foobar"}
print(foo[1])
print(foo[2])
print(foo[3])
table.insert(foo, "barfoo")
print(foo[4])

stack = {}

function stack.push(item)
        table.insert(stack, 1, item)
end

function stack.pop()
        local value = stack[1]
        table.remove(stack, 1)
        return value            jj
end

return stack

--

local stack = require("stack")

stack.push(1)
stack.push(2)
stack.push(3)

assert(stack.pop() == 3)
assert(stack.pop() == 2)
stack.push(72)
assert(stack.pop() == 72)
assert(stack.pop() == 1)
assert(stack.pop() == nil)

```

## node
```
function browse_tree(n)
	print(n.data)
	if n.next then
		return browse_tree(n.next)	
	end
end



local n1 = {}
n1["data"] = 1
n1["next"] = nil

browse_tree(n1)


print("---")


local n2 = {}
n2["data"] = 2

n1["next"] = n2
n2["next"] = nil

browse_tree(n1)
```

## nooop
```
local Canvas = {}

function Canvas.new(width, height)
        local self = {}
        self.width = width
        self.height = height
        return self
end

return Canvas

-- 

local Canvas = require("Canvas")
local c = Canvas.new(80, 60)

local c2 = Canvas.new(40, 30)


assert(c.width == 80)
assert(c2.width == 40)

c2.width = 60
assert(c2.width == 60)
assert(c.width == 80)
```

