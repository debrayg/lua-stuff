-- ////////////////////////////  helpful functions \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
function isnum(char)
	if tonumber(char) ~= nil then return true end
	return false
end


function isalpha(char)
	if isnum(char) then return false end
	return true
end


-- ////////////////////////////  read file \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
-- local file = io.open("in.txt", "r")
-- local program = file:read("*all")
-- io.close(file)


-- program = "81+2*4-3*8+5-4+(745-4-5)*9-(87+5*3+(8-5*4))"


local program = "foo = 8+1*3-(14+63)*2+1*2-(1+5)+9*8 ;"



assert(not isnum("a"))
assert(isalpha("a"))

assert(not isalpha("4"))
assert(isnum("4"))

assert(isalpha("+"))
assert(not isnum("+"))

assert(not isnum("("))
assert(isalpha("("))

assert(not isnum(";"))
assert(isalpha(";"))


assert(isalpha(" "))
assert(not isnum(" "))


-- ////////////////////////////  lexer  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
function lex(program)
	local index = 1
	
	local buf = ""
	local cur_tok = nil
	local tokens = {}
	
	while index <= #program+1 do


		if index==#program+1 then -- if program is like so "a = 12 * 21 + 3" we will reach the end and not add 3. So we had it now
			if cur_tok and (cur_tok == "num" or cur_tok == "id" ) then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end

			break
		end
	
		if program:sub(index, index) == " " then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end
			index = index + 1
			cur_tok = nil
	
		elseif program:sub(index, index) == "\n" then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end
			index = index + 1
			cur_tok = nil

		elseif program:sub(index, index) == "=" then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end
			local token = {}
			token.tp = "op"
			token.content = "="
			table.insert(tokens, token)
			index = index + 1
			cur_tok = nil

		elseif program:sub(index, index) == "+" then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end

			local token = {}
			token.tp = "op"
			token.content = "+"
			token.opnb = 2
			table.insert(tokens, token)
			index = index + 1
			cur_tok = nil

		elseif program:sub(index, index) == "-" then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end

			local token = {}
			token.tp = "op"
			token.content = "-"
			token.opnb = 2
			table.insert(tokens, token)
			index = index + 1
			cur_tok = nil

		elseif program:sub(index, index) == "*" then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end
			local token = {}
			token.tp = "op"
			token.content = "*"
			token.opnb = 3
			table.insert(tokens, token)
			index = index + 1
			cur_tok = nil

		elseif program:sub(index, index) == "/" then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end

			local token = {}
			token.tp = "op"
			token.content = "/"
			token.opnb = 3

			table.insert(tokens, token)
			index = index + 1
			cur_tok = nil

		elseif program:sub(index, index) == ";" then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end
			local token = {}
			token.tp = "sep"
			token.content = "sep"

			table.insert(tokens, token)
			index = index + 1
			cur_tok = nil

		elseif program:sub(index, index) == "(" then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end
			local token = {}
			token.tp = "LPAR"
			token.content = "("
			table.insert(tokens, token)
			index = index + 1
			cur_tok = nil

		elseif program:sub(index, index) == ")" then
			if cur_tok then
				local token = {}
				token.tp = cur_tok
				token.content = buf
				table.insert(tokens, token)
				buf = ""
			end
			local token = {}
			token.tp = "RPAR"
			token.content = ")"
			table.insert(tokens, token)
			index = index + 1
			cur_tok = nil
	
		elseif isnum(program:sub(index, index)) then
			if cur_tok then
				if cur_tok == "num" then
					buf = buf..program:sub(index, index)
				elseif cur_tok == "id" then
					buf = buf..program:sub(index, index)
				else 
					error("expected num or id but got : "..cur_tok)
				end
			else
				cur_tok = "num"
				buf = buf..program:sub(index, index)
			end
			index = index + 1
	
		elseif isalpha(program:sub(index, index)) then
			if cur_tok then
				if cur_tok == "id" then
					buf = buf..program:sub(index, index)
				else 
					error("Synt err ; expected token type id but found : "..cur_tok.." at position "..index)
				end
			else 
				cur_tok = "id"
				buf = buf..program:sub(index, index)
			end
			index = index + 1
		else
			error("Synt err : unrecognized character : "..program:sub(index, index).." at position : "..index)
		end
	end
	
	local eof_tok = {}
	eof_tok.tp = "eof"
	eof_tok.content = "eof"
	table.insert(tokens, eof_tok)

	return tokens
end

-- //////////////////////////ls//  parser \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

-- GRAMMAR : 
-- L -> I;L | eof
-- I -> id = E
-- E = T + E | T - E | T 
-- T = F*T | F/T | F
-- F -> id | nb | (E)

-- note : we don't accept empty statement in program like so 'a = 72; ;'

local pos = 1
function parse(tokens)
	return L(tokens)
end





-- L -> I;L | eof
function L(tokens) 
	local node = {}
	node.name = "L"

	if tokens[pos].tp == "eof" then
		print("found eof, parsing OK")
		node.name = "eof"
		return node
	else 
		node.left = I(tokens)

		if tokens[pos].tp == "sep" then
			pos = pos + 1
			node.right = L(tokens)
		else 
			error("parsing error (1) : expected sep, got "..tokens[pos].tp.." "..tokens[pos].content.."at position : "..pos)
		end
	end

	return node
end



-- I -> id = E
function I(tokens)
	local node = {}
	node.name = "I"
	if tokens[pos].tp == "id" then
		node.left = tokens[pos].content
		pos = pos + 1
		if tokens[pos].content == "=" then
			pos = pos + 1
			node.right = E(tokens)
		else 
			error("parsing error (2) : expected assign, got "..tokens[pos].tp.." "..tokens[pos].content.." at position : "..pos)
		end
	else 
		error("parsing error (3) : expected id, got "..tokens[pos].tp.." "..tokens[pos].content.." at position : "..pos)
	end


	return node

end




-- E = T + E | T - E | T 
function E(tokens)
	local node = {}

	node.name = "E"
	node.left = T(tokens)
	
	if tokens[pos].tp == "op" then
		if tokens[pos].content == "+" or tokens[pos].content == "-" then
			node.op = tokens[pos].content
			pos = pos + 1
			node.right = E(tokens)
		else
			error("sem err "..tokens[pos].content)
		end
	end

	return node
end




-- T = F*T | F/T | F
function T(tokens)
	local node = {}

	node.name = "T"
	node.left = F(tokens)
	
	if tokens[pos].tp == "op" then
		if tokens[pos].content == "*" or tokens[pos].content == "/" then
			node.op = tokens[pos].content
			pos = pos + 1
			node.right = T(tokens)
		end
	end

	return node
end






-- F =  id | nb | (E)
function F(tokens)
	local node = {}

	node.name = "F"

	if tokens[pos].tp == "id" then

		node.left = tokens[pos].content
		pos = pos + 1

	elseif tokens[pos].tp == "num" then

		node.left = tokens[pos].content
		pos = pos + 1

	elseif tokens[pos].content =="(" then
		pos = pos + 1
		node.right = E(tokens)

		if tokens[pos].content==")" then
			pos = pos + 1
		else 
			error("parsing error (4): expected ')' but got "..tokens[pos].tp.." "..tokens[pos].content.." at position "..pos)
		end
	else 
		error("parsing error error (5) : expected id or num or '(' but got "..tokens[pos].tp.." "..tokens[pos].content.." at position : "..pos)
	end

	return node
end

-- ////////////////////////////  reverse polish \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


function rpn(tokens) 
	local index = 1
	local queue = {}
	local opstack = {} 

	while index <= #tokens do
		if tokens[index].tp == "eof" then
			break

		elseif tokens[index].tp == "num" then
			table.insert(queue, tokens[index])
			index = index + 1
	
		elseif tokens[index].tp == "op" then
	
			if #opstack == 0 then
				table.insert(opstack, 1, tokens[index])
				index = index + 1
			elseif opstack[1].tp == "LPAR"  then
				table.insert(opstack, 1, tokens[index])
				index = index + 1
			else 
				while #opstack > 0 and opstack[1].tp ~= "LPAR" and opstack[1].opnb >= tokens[index].opnb  do
			 		table.insert(queue, opstack[1])
			 		table.remove(opstack, 1)
				end
				table.insert(opstack, 1, tokens[index])
				index = index + 1
			end
	
		elseif tokens[index].tp == "LPAR" then
			table.insert(opstack, 1, tokens[index])
			index = index + 1
		elseif tokens[index].tp == "RPAR" then
			while (opstack[1].tp ~= "LPAR") do
				table.insert(queue, opstack[1])
				table.remove(opstack, 1)
			end
			table.remove(opstack, 1)
			index = index + 1
		else 
			error("expected eof or num or op or LPAR or RPAR but found : "..tokens[index].left)
		end
	end
	
	while #opstack > 0 do
		table.insert(queue, opstack[1])
		table.remove(opstack, 1)
	end
	return queue

end


function solve_rpn(queue)
	local solve_stack = {}
	local index = 1
	
	while index <= #queue do
	
		if queue[index].tp == "num" then
			table.insert(solve_stack, 1, tonumber(queue[index].left))
		else 
	
			local op = queue[index].content
	
			local right = solve_stack[1] 
			table.remove(solve_stack, 1)
	
			local left = solve_stack[1] 
			table.remove(solve_stack, 1)
	
			if op == "+" then
				table.insert(solve_stack, 1 , left + right)
			elseif op == "-" then
				table.insert(solve_stack, 1 , left - right)
			elseif op == "*" then
				table.insert(solve_stack, 1 , left * right)
			elseif op == "/" then
				table.insert(solve_stack, 1 , left / right)
			else 
				error("expected + or - or * or / while solving queue but got "..queue[index].left)
			end
		end
		index = index + 1
	end
        return solve_stack[1]	
end

local spacing = {}
for i=1, 5 do
	spacing[i] = ""
end

function plot(node)
	if node then
		if node.name == "L" then
			print(spacing[1]..node.name)
			spacing[1] = spacing[1].." "
		elseif node.name == "eof" then
			print("EOF")
		elseif node.name == "I" then
			print(spacing[1]..node.name.."("..node.content..")")
			spacing[2] = spacing[2].." "
		elseif node.name == "E" then
			print(spacing[3]..node.name)
			spacing[3] = spacing[3].." "
		elseif node.name == "T" then
			print(spacing[4]..node.name)
			spacing[4] = spacing[4].." "
		elseif node.name == "F" then
			if node.content then
				print(spacing[5]..node.name.."("..node.content..")")
				spacing[5] = spacing[5].." "
			else 
				print(spacing[5]..node.name)
				spacing[5] = spacing[5].." "
			end
		else
			error("unreachable")
		end
		plot(node.left)
		plot(node.right)
	end
end

-- calculate E node ; doesn't recognize L or I nodes
-- BUG WHEN USING MINUS ??
function evaluate(node)
	if node.name == "E" then
		if node.left and node.right then
			if node.op == "+" then
				return evaluate(node.left) + evaluate(node.right)
			elseif node.op == "-" then
				-- FIXME : bug when - ; wrong solution
				return evaluate(node.left) - evaluate(node.right)
			else
				error("unreachable 4")
			end
		elseif node.left then
			return evaluate(node.left)
		else
			error("unreachable 1")
		end
	elseif node.name == "T" then
		if node.left and node.right then
			if node.op == "*" then
				return evaluate(node.left) * evaluate(node.right)
			elseif node.op == "/" then
				return evaluate(node.left) / evaluate(node.right)
			else
				error("unreachable 5")
			end
		elseif node.left then
			return evaluate(node.left)
		else
			error("unreachable 2")
		end
	elseif node.name == "F" then
		if node.right and not node.left then
			return evaluate(node.right)
		elseif node.left and not node.right then
			return node.left
		else
			error("unreachable 6")
		end
	else
		error("unreachable 3")
	end
end


print(program)
local tokens = lex(program)
-- local queue = rpn(tokens)
-- solve_rpn(queue)


print("----------------------------------------------------------------------")
for i=1, #tokens do
	print(i.." -> "..tokens[i].tp.." ______ "..tokens[i].content)
end
print("----------------------------------------------------------------------")

local parse_tree = parse(tokens)
assert(parse_tree.left.right.name == "E")
print(evaluate(parse_tree.left.right))







local program2 = "8+1*3-(14+63)*2+1*2-(1+5)+9*8"
local tokens2 = lex(program2)
local queue = rpn(tokens2)
print(solve_rpn(queue))
